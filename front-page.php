<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<?php putRevSlider( 'homepage' ); ?>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">

		    	<div id="submenu">
		    		<a href="/services/" id="left">
		    			<p><strong>Clinic Services</strong>Conveniently Located off the I-10</p>
		    		</a>
		    		<a href="/services/" id="mid">
		    			<p><strong>On Site Services</strong>We'll Come To Your Business</p>
		    		</a>
		    		<a href="/contact-us/" id="right">
		    			<p><strong>Contact Us Today</strong>Schedule an Appointment Today</p>
		    		</a>
		    	</div>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'homepage' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>