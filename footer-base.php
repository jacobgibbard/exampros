					<footer class="footer" role="contentinfo">
						<div class="center">
							<a href="/"><img src="/wp-content/uploads/dotexam-logo.jpg" /></a>
						</div>
						<div id="inner-footer" class="row">
							<div class="large-8 medium-12 columns text-left">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
							<div class="large-4 medium-12 columns text-right">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->