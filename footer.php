					<div class="row testimonials-wrap">
					  <div class="columns large-3 medium-3 hide-for-small-only">
					  	<img alt="You don't have to take our word for it..." src="/wp-content/uploads/home-circled-message.png" />
					  	<a href="/testimonials" class="outlinebutton">View All Testimonials</a>
					  </div>
					  <div class="columns large-9 medium-9 small-12">
					 	<?php echo do_shortcode('[show_testimonials]') ?>
					  </div>
					</div>
					<footer class="footer" role="contentinfo">
						<div class="center">
							<a href="/"><img src="/wp-content/uploads/dotexam-logo.jpg" /></a>
						</div>
						<div id="inner-footer" class="row">
							<div class="large-8 medium-12 columns text-left">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
							<div class="large-4 medium-12 columns text-right">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->