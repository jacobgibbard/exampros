<div class="top-bar" id="main-menu">
	<div class="row">
		<div class="large-4 medium-3 hide-for-small-only columns logo">
			<a href="/"><img src="/wp-content/uploads/dotexam-logo.jpg" id="site-logo" /></a>
		</div>
		<div class="show-for-small-only small-12 columns small-logo">
			<a href="/"><img src="/wp-content/uploads/dotexam-logo.jpg" id="site-logo" /></a>
			<a class="toggleness" data-toggle="off-canvas"><i class="fa fa-bars"></i></a>
		</div>
		<div class="large-8 medium-9 small-12 columns text-right">
			<h4 class="hide-for-small-only">Call Us Today</h4>
			<h2 class="hide-for-small-only">(909) 333-6803</h2>
			<p class="hide-for-small-only">1894 Commercenter Dr. West, STE 106  San Bernardino, CA 92408</p>
			<div class="show-for-small-only text-center">
				<div class="menu-centered">
				  <ul class="menu contact-small-menu">
				    <li><a href="tel:9093336803"><i class="fa fa-mobile"></i> (909) 333-6803</a></li>
				    <li><a href="/contact/"><i class="fa fa-map-marker"></i> Directions</a></li>
				  </ul>
				</div>
			</div>
			<div class="main-menu"><?php joints_top_nav(); ?></div>
		</div>
		<div class="show-for-medium-only medium-12 columns">
			<?php joints_top_nav(); ?>
		</div>		
	</div>
</div>